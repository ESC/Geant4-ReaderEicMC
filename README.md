EicMC
=========

Introduction
------------
  This example demonstrates hot to read events from EicMC data files.
The example is adapted from HepMCEx02 Geant4 example.

The only relevant/dedicated code is class `EicMCG4Interface` used 
used by the primary generator action.

Dependency
----------
The goal of the example is to show howo to read input file
from generators saved in EicMC format without the need of external
libraries (beside google protocol buffer): *towards a self-describing
data format*.

Dependency on input data format
-----------------------
The data structure is described in a google protocol buffer file. 
The file can be created reading the input file itself (self-describing?).  
Starting from EicMC package we use the `bootstrap` application to 
read an input file `.eicmc` and create correspongind `.proto` file.   
This file is provided to the `protoc` application 
(included with google protocol buffer) to generate the C++ classes
that will read the `.eicmc` files. In this example the integration with `protoc` is
done via cmake so you do not have to worry. Together with this source
code a version of the `.proto` file is provided (no need to run
`bootstrap`).

Software dependencies
-------------------
 1. Geant4 (version 10.3)
 2. Google protocol buffer 3 (tested with 3.3)
 3. A c++11 compatible compiler (you need it anyway for G4). e.g. gcc >= 4.8.3

How to use
----------
 1. **Optional generation of `.proto` data**:
	```
	bootstrap inputfile.eicmc > data.proto
	```
    Proto file must be called `data.proto`
 2. **Compilation**:
    Assuming Geant4 is installed in `<g4dir>` and google protocol 
    buffer is installed in `<proto>` (if it is in a location reachable
    via `$LD_LIBRARY_PATH` it should work without specifying anything) 
    
	    //get this code and put in $PWD/source
	    mkdir build && cd build  
	    cmake -DGeant4_DIR=<g4dir>/lib[64]/Geant4-* -DEICMCPROTO=<data.proto-full-path> \
	    	[-DPROTOBUF_LIBRARY=<proto>/lib/libprotobuf.so -DPROTOBUF_INCLUDE_DIR=<protobuf>/include] \
	    	$PWD/source
	    make 

 2. **Use**:
    `./EicMC` to open up graphics (if enabled in G4), and type at the 
    Geant4 prompt:
	    
	    /generator/open data/pythia6.eicmc
	    /run/beamOn 1
	    
     Alternatively, in batch mode: `./EicMC eicmc.mac`
 


Original README file
--------------------
This is a cut-and-paste of the original README file, containing
additional information about other aspects of the code.



 
  This example demonstrates how to interface primary particles in Geant4
with various event generators via the HepMC Monte Carlo event interface.
This is another example having the same generator action as HepMCEx01, 
but much simpler user control.

1. Primary Generator

 H02PrimaryGeneratorAction has HepMCG4Interface as the generator.
There are two types of generators provided as samples. One generator reads 
primary information from a HepMC Ascii file (data/example_MyPythia.dat).
The other one generates primaries directly invoking PYTHIA routines 
in every event.

2. Geometry

  A simplified collider-type geometry, which consists of 
    - endcap calorimeter (a set of tubes filled with lead), 
    - barrel calorimeter (tube filled with lead),
    - barrel muon detector (8 sets of plates filled with Ar),
    - endcap muon detecror, (a set of tubes filled with Ar) and
    - uniform magnetic field along the z axis of 3 Tesla at the 
      central region.

3. Physics List 

  FTFP_BERT predefined physics list

4. User actions

  All particles except muons are killed in the calorimeter section.

5. Installation
 
 See HepMC/README how to build this example.

6. Execution
 
 % HepMCEx02 hepmc_pygen.in
   
