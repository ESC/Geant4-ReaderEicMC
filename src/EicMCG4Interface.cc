/*
 * EicMCG4Interface.cc
 *
 *  Created on: May 2, 2017
 *      Author: adotti
 */
#include <algorithm>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/coded_stream.h>

#include "G4LorentzVector.hh"
#include "G4SystemOfUnits.hh"
#include "G4PrimaryVertex.hh"
#include "G4Event.hh"

#include "EicMCG4Interface.hh"

#include "data.pb.h"

void EicMCG4Interface::Initialize() {
  if ( verbose > 1 ) G4cout<<"EicMCG4Interface::Initialize"<<G4endl;
  if ( m_fname.empty() ) {
      G4Exception("EicMCG4Interface::Initialize","ExGProto001",FatalException,"No input file specified");
  }
  m_file.reset( new std::ifstream(m_fname,std::ios::binary) );
  if ( ! m_file->is_open() ) {
      G4Exception("EicMCG4Interface::Initialize","ExGProto001",FatalException,"Cannot open input file");
  }
  m_zstream.reset( new google::protobuf::io::IstreamInputStream(m_file.get() ) );
  m_stream.reset( new GIS(m_zstream.get() ) );
}

void EicMCG4Interface::Finalize() {
  if ( verbose > 1 ) G4cout<<"EicMCG4Interface::Finalize"<<G4endl;
  if (m_file != nullptr && m_file->is_open() ) m_file->close();
}

namespace {
  inline G4ThreeVector Position(const eicmc::Record::MonteCarloEvent::Particle& in ) {
    return G4ThreeVector(in.vx()*cm,in.vy()*cm,in.vz()*cm);
  }

  G4PrimaryVertex* Convert(const eicmc::Record::MonteCarloEvent::Particle& in ) {
    //What are unites: [cm,GeV,ns] from README of EicMC
    //What is the event vertex???? USe multiple vertexes.
    const auto vtx = new G4PrimaryVertex( Position(in) , in.t0()*ns );
    //Wrong, need to get mass from event (due to resonances) see how is done in original
    //example
    vtx->SetPrimary(new G4PrimaryParticle(in.pdg(),in.px()*GeV,in.py()*GeV,in.pz()*GeV));
    return vtx;
  }

}

void EicMCG4Interface::GeneratePrimaryVertex(G4Event* anEvent)
{
  if (m_file == nullptr || !m_file->is_open() ) Initialize();

  using namespace google::protobuf;

  //Copy from Alexander's code

  bool abort = true;

  //Loop until a correct type record is read, at that point break this loop
  while ( true ) {
      // Yes, one coded stream per every new message, otherwise will hit 512MB limit
      // sooner or later; the protobuf guys claim this call is cheap though :-);
      io::CodedInputStream cistream(m_stream.get());

      // Get record delimiter;
      uint64_t delimiter = 0;
      if (!cistream.ReadVarint64(&delimiter)) {
	  //TODO: Ask Alexander meaning of this, looks like an error
	  Finalize();
	  G4Exception("EicMCG4Interface::GeneratePrimaryVertex(...)","ExGProto002",RunMustBeAborted,"Bad input file format, cannot find delimiter");
	  break;//This is needed to break the loop and let G4 handle the exception (trying to be gracious)
      }

      if ( delimiter == 0 ) {
	  Finalize();
	  G4Exception("EicMCG4Interface::GeneratePrimaryVertex(...)","ExGProto003",RunMustBeAborted,"No more events");
	  break;
      }

      // NB: bits [0..7] encode event record type as in eicmc.proto 'oneof RecordCore';
      uint64_t recsize = delimiter >> 8, rectype = delimiter & 0xFF;



      if ( verbose > 1 ) G4cout<<"EicMCG4Interface::GeneratePrimaryVertex(...) : Record size="<<recsize<<" with type="<<rectype<<G4endl;
      // Here we want to select event records only;
      if (rectype == eicmc::Record::kMcevent) {
	  io::CodedInputStream::Limit limit = cistream.PushLimit(recsize);

	  eicmc::Record record;
	  if (!record.MergeFromCodedStream(&cistream) || !cistream.ConsumedEntireMessage()) {
	      //TODO: Ask Alexander meaning of this
	      Finalize();
	      G4Exception("EicMCG4Interface::Initialize","ExGProto002",RunMustBeAborted,"Bad input file format");
	      break;
	  }

	  cistream.PopLimit(limit);

	  // Just a dummy consistency check (see "rectype" selection above);
	  assert(record.has_mcevent());
	  const auto &mcevent = record.mcevent();

	  if ( verbose > 0 ) {
	      G4cout<<"EicMCG4Interface::GeneratePrimaryVertex(...) : particles_size="
		  <<mcevent.particles_size()
		  <<" x_bj="<<mcevent.true_x()<<" Q^2="<<mcevent.true_q2();
	      // Show access to the generator-specific variables (PYTHIA6 in this case);
	      if ( mcevent.has_pythia6() ) {
		  G4cout<<" PYTHIA6 true Y="<<mcevent.pythia6().true_y();
	      }
	      G4cout<<G4endl;
	  }

	  // Loop through all particles; see eicmc.proto file for other fields;
	  const auto& particles = mcevent.particles();

	  std::for_each( particles.begin() , particles.end(),
	    [this,anEvent](const eicmc::Record::MonteCarloEvent::Particle& part) {
//	      if (this->verbose > 0 ) G4cout<<"EicMC="<<part.SerializeAsString();
	      if ( part.status() == 1 && this->CheckVertexInsideWorld(Position(part))) {
		  auto vtx = Convert(part);
		  if ( this->verbose > 0 ) G4cout<<" ===> Pos="<<vtx->GetPosition()<<", Mom="<<vtx->GetPrimary(0)->GetMomentum()<<", "<<vtx->GetPrimary(0)->GetPDGcode()<<G4endl;
		  anEvent->AddPrimaryVertex(vtx);
	      }
	  });
	  //An event has been decoded, break infinite loop
	  break;
      }
      else {
	  G4Exception("EicMCG4Interface::GeneratePrimaryVertex(...)","ExGProto001",JustWarning,"Retrieved non-event record, skipping");
	  // Non-event record: just skip without decoding;
	  cistream.Skip(recsize);
      }
  }//Infinite loop
}
