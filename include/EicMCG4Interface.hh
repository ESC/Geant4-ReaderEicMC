/*
 * EicMCG4Interface.hh
 *
 *  Created on: May 2, 2017
 *      Author: adotti
 */

#ifndef INCLUDE_EICMCG4INTERFACE_HH_
#define INCLUDE_EICMCG4INTERFACE_HH_

#include <string>
#include <fstream>
#include <memory>
#include "G4VPrimaryGenerator.hh"

#include <google/protobuf/io/gzip_stream.h>
#include <google/protobuf/io/zero_copy_stream.h>

#include "data.pb.h"

class EicMCG4Interface : public G4VPrimaryGenerator
{
public:
  EicMCG4Interface () : verbose(0) , m_fname("") { }
  virtual ~EicMCG4Interface () { Finalize(); }
  virtual void GeneratePrimaryVertex(G4Event* anEvent) override;

  void Initialize();
  void Finalize();

  void SetFileName(const std::string& fn ) { m_fname = fn; }
  void SetVerbose(G4int ve) { verbose = ve; }

protected:
  using GIS=google::protobuf::io::GzipInputStream;
  using ZCIS=google::protobuf::io::ZeroCopyInputStream;
  G4int verbose;
  std::string m_fname;
  std::unique_ptr<GIS> m_stream;
  std::unique_ptr<ZCIS> m_zstream;
  std::unique_ptr<std::ifstream> m_file;
};

#endif /* INCLUDE_EICMCG4INTERFACE_HH_ */
